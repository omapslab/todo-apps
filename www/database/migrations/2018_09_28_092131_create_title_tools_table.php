<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTitleToolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('title_tools', function (Blueprint $table) {
            $table->increments('id');
            $table->string('categories');
            $table->string('genre');
            $table->string('title');
            $table->string('subtitle');
            $table->string('cover_img');
            $table->string('author_name');
            $table->string('performer_name');
            $table->string('isbn');
            $table->integer('price');
            $table->integer('rating');
            $table->string('duration');
            $table->string('discs');
            $table->string('publisher');
            $table->dateTime('publication_date');
            $table->string('abridged');
            $table->string('series_name');
            $table->string('synopsis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('title_tools');
    }
}
