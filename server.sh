#!/bin/sh

# Server Runner
# @author Agus prasetyo ~ OMAPSLAB 
# @email agusprasetyo811@gmail.com

cd www
chmod -R o+w storage/*
cd ..
docker-compose up -d
