
TODO APPS
====

This is sample apps TODO build with Laravel & Vuejs


Technology :

- Laravel 5.6
- VueJS v2
- Docker 18.06.1-ce
- PostCSS 
- SCSS
- Webpack 4

Access Link
- http://178.128.109.67:4400/todo (Apps)
- http://178.128.109.67:8880 (PMA root:root)

CMD Helper
- `./server.sh` (Running Server)
- `./init.sh` (Init Setting Laravel)
- `./inspect.sh` (List IP of Docker container)

STEP
- Running `./server.sh`
- Check PMA via http://178.128.109.67:8880 (PMA root:root)
- Running `./init.sh`
- Running `./inspect.sh` to get Host of Mysql See container that contain `_db` 
- setting `.env` from `www` dir
- running apps http://178.128.109.67:4400/todo
