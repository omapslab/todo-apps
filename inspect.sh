#!/bin/sh

# Server Runner
# @author Agus prasetyo ~ OMAPSLAB 
# @email agusprasetyo811@gmail.com

docker inspect -f '{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq)
